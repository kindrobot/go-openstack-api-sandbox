package main

import (
	"fmt"
	"os"
	"time"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/flavors"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/images"
	"github.com/gophercloud/gophercloud/openstack/networking/v2/networks"
	"github.com/gophercloud/gophercloud/pagination"
)

func main() {
	opts, err := openstack.AuthOptionsFromEnv()
	if err != nil {
		fmt.Println(err)
		return
	}

	provider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		fmt.Println(err)
		return
	}

	computeClient, err := openstack.NewComputeV2(provider, gophercloud.EndpointOpts{
		Region: os.Getenv("OS_REGION_NAME"),
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	networkClient, err := openstack.NewNetworkV2(provider, gophercloud.EndpointOpts{
		Region: os.Getenv("OS_REGION_NAME"),
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	// List servers
	pager := servers.List(computeClient, servers.ListOpts{})

	fmt.Println("\nServers:")
	err = pager.EachPage(func(page pagination.Page) (bool, error) {
		serverList, err := servers.ExtractServers(page)
		if err != nil {
			return false, err
		}

		for _, s := range serverList {
			fmt.Println(s.Name)
		}
		return true, nil
	})

	if err != nil {
		fmt.Println(err)
	}

	// Get and print server information for server named 'control-plane'
	serverPager := servers.List(computeClient, servers.ListOpts{Name: "control-plane"})
	err = serverPager.EachPage(func(page pagination.Page) (bool, error) {
		serverList, err := servers.ExtractServers(page)
		if err != nil {
			return false, err
		}

		fmt.Println("\nServer information for 'control-plane':")
		for _, s := range serverList {
			fmt.Printf("ID: %s\n", s.ID)
			fmt.Printf("Name: %s\n", s.Name)
			fmt.Printf("Status: %s\n", s.Status)
			fmt.Printf("Created: %s\n", s.Created)
			// Add more fields here as you need

			for networkName, networkInfo := range s.Addresses {
				fmt.Printf("Network Name: %s, IPs: %v\n", networkName, networkInfo)
			}
		}

		return true, nil
	})

	// List images
	imagePager := images.ListDetail(computeClient, images.ListOpts{})
	err = imagePager.EachPage(func(page pagination.Page) (bool, error) {
		imageList, err := images.ExtractImages(page)
		if err != nil {
			return false, err
		}
		fmt.Println("\nImages:")
		for _, i := range imageList {
			fmt.Printf("Name: %s, ID: %s\n", i.Name, i.ID)
		}
		return true, nil
	})

	// List flavors
	flavorPager := flavors.ListDetail(computeClient, flavors.ListOpts{})
	err = flavorPager.EachPage(func(page pagination.Page) (bool, error) {
		flavorList, err := flavors.ExtractFlavors(page)
		if err != nil {
			return false, err
		}
		fmt.Println("\nFlavors:")
		for _, f := range flavorList {
			fmt.Printf("Name: %s, ID: %s\n", f.Name, f.ID)
		}
		return true, nil
	})

	// List networks
	networkPager := networks.List(networkClient, networks.ListOpts{})
	err = networkPager.EachPage(func(page pagination.Page) (bool, error) {
		networkList, err := networks.ExtractNetworks(page)
		if err != nil {
			return false, err
		}
		fmt.Println("\nNetworks:")
		for _, n := range networkList {
			fmt.Printf("Name: %s, ID: %s\n", n.Name, n.ID)
		}
		return true, nil
	})

	// Define user data (startup script)
	userData := `#!/usr/bin/env bash
curl -sfL https://get.k3s.io | sh -`

	// Specify server creation options
	serverCreateOpts := servers.CreateOpts{
		Name:      "test-" + fmt.Sprintf("%v", time.Now().Unix()),
		ImageRef:  "d753f466-40e4-471d-ba4b-7cbab78d827b", // debian-12.0-bookworm
		FlavorRef: "55d5d90f-c5c6-44ff-bb8a-be7b077481cf", // g3.cores1.ram2.disk20
		Networks: []servers.Network{
			{UUID: "7425e328-560c-4f00-8e99-706f3fb90bb4"}, // lan-flat-cloudinstances2b
		},
		UserData: []byte(userData),
	}

	// Create server
	server, err := servers.Create(computeClient, serverCreateOpts).Extract()
	if err != nil {
		fmt.Println("\nFailed to create server:", err)
		return
	}

	fmt.Printf("\nServer %s has been created successfully with ID %s\n", server.Name, server.ID)

}

