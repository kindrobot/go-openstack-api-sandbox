Run me with

```
source /var/openstack/app-cred-catalyst-openrc.sh
go run main.go
```

Should see something like:
```
Servers:
test-1697665075
test-1697664992
control-plane

Server information for 'control-plane':
ID: be8870d5-bea8-40ea-b821-864b43836f34
Name: control-plane
Status: ACTIVE
Created: 2023-10-11 15:55:48 +0000 UTC
Network Name: lan-flat-cloudinstances2b, IPs: [map[OS-EXT-IPS-MAC:mac_addr:fa:16:3e:9a:75:5f OS-EXT-IPS:type:fixed addr:172.16.7.79 version:4]]

Images:
Name: debian-12.0-bookworm, ID: d753f466-40e4-471d-ba4b-7cbab78d827b
Name: debian-11.0-bullseye, ID: 1e5b2120-1b20-4d1a-93dc-f88c3d0de54c
Name: debian-11.0-bullseye (deprecated 2023-06-08), ID: e69cb6f7-e5c7-41de-b08d-8e5739c20de3
Name: magnum-fedora-coreos-34, ID: 89f6c455-36bd-4d41-8d63-f48cfb50a3fb
Name: debian-11.0-bullseye (deprecated 2023-01-12), ID: 9a01d3d8-e793-4775-8b81-434f68c687a7
Name: debian-11.0-bullseye (deprecated 2022-05-18), ID: ec5c0bc8-53a4-4df9-98c5-91cd699288e1
Name: debian-10.0-buster, ID: 261865bc-7ed5-4df0-be08-80657a388514
Name: debian-10.0-buster (deprecated 2021-07-30), ID: 844021a6-5837-42b8-8a38-b7547be9e952
Name: debian-10.0-buster (deprecated 2021-03-28), ID: d633c111-00d1-4e72-bb3b-c2bd60518f4a
Name: debian-10.0-buster (deprecated 2021-03-24), ID: e971dc0f-3b5c-4cd2-ab8b-02faf403c136
Name: debian-10.0-buster (deprecated 2021-03-01), ID: 64351116-a53e-4a62-8866-5f0058d89c2b
Name: debian-10.0-buster (deprecated 2021-02-22), ID: 6b67c8a1-6356-464d-a885-0576d7263e51
Name: debian-10.0-buster (deprecated 2020-10-16), ID: 031d2d76-8368-4066-a502-d28107d0195e
Name: debian-10.0-buster (deprecated 2019-12-18), ID: fc6fb78b-4515-4dcc-8254-591b9fe01762
Name: debian-10.0-buster (deprecated 2019-12-15), ID: b6b58ba2-8656-49b4-af13-d0530ac05365
Name: debian-10.0-buster (deprecated 2019-07-30), ID: a64f590c-3ed7-43e1-a592-b44d86f10641
Name: debian-10.0-buster (deprecated 2019-07-29), ID: 374ca3c6-6c4b-4e03-9f31-52e0d44aae0c
Name: debian-10.0-buster-prerelease, ID: 5cd88504-5e2b-4f63-b18e-2ec935c914dd

Flavors:
Name: g3.cores16.ram16.disk20, ID: 22ef70d9-cc89-4090-a202-27b1bf79b5f1
Name: g3.cores8.ram16.disk20, ID: 35ad2501-a87b-4e50-93da-e03b4724646c
Name: g3.cores1.ram2.disk20, ID: 55d5d90f-c5c6-44ff-bb8a-be7b077481cf
Name: g3.cores8.ram24.disk20.ephemeral90.4xiops, ID: 736b4da6-4161-4285-ac8f-bfc118b7c459
Name: g3.cores16.ram34.disk20, ID: 7dd33202-32c3-4bc7-b2d4-10c2ebe7e5c5
Name: g3.cores8.ram32.disk20, ID: 84e084ae-b2a2-4efb-abb2-0afa3b7067e0
Name: g3.cores2.ram4.disk20, ID: bb8bee7e-d8f9-460b-8344-74f745c139b9
Name: g3.cores1.ram1.disk20, ID: bf48880d-0c1b-4c2a-8e8b-778d28b16561
Name: g3.cores4.ram8.disk20, ID: c14b5856-5e6a-4f7a-8125-ace4f616c299

Networks:
Name: wan-transport-eqiad, ID: 5c9ee953-3a19-4e84-be0f-069b5da75123
Name: lan-flat-cloudinstances2b, ID: 7425e328-560c-4f00-8e99-706f3fb90bb4

Server  has been created successfully with ID b0d43e70-a3dd-4803-9ade-800dd20d14ab
```
